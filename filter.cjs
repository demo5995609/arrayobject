// let arr = require('./arrays.cjs');

function filter(arr, cb, num) {
    let result = [];
    for(let idx = 0; idx<arr.length; idx++){
        let val = arr[idx];
        if(cb(val, idx, arr) === true){
            result.push(val);
        }
    }
    return result;
}

module.exports = filter;