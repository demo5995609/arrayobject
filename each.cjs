let arr = require('./arrays.cjs');

function callback(num) {
    num = num + 10;
    return num;
}

function eachFunction(arr, cb) {
    
    for(let idx = 0; idx<arr.length; idx++){
        arr[idx] = cb(arr[idx]);
    }
    return arr;
}

let result = eachFunction(arr, callback);
console.log(result);
