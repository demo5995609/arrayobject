// const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

let newRes = [];
function flatten(nestedArray, depth = 1){

    let flatArray = [];

    nestedArray.forEach(element => {
        if(depth == 0){
            newRes.push(element);
        }
        else if(Array.isArray(element)){
            flatArray = flatArray.concat(flatten(element, depth-1));
        }
        else{
            newRes.push(element);
        }
    });

    return newRes;
}
// [1, 2, [3], [[4]]
module.exports = flatten;
// let result = flatten(nestedArray);
// console.log(result);
// []
// []