let val = require('./arrays.cjs');
let arr = val.items;
let findFunction = require('./find.cjs');
// arr.nestedArray
// arr.items
function callback(num, arr, index) {
    for(let idx = 0; idx<arr.length; idx++){
        if(arr[idx] === num){
            return true;
        }
    }
    return false;
}
let num = 10;
let result = findFunction(num, callback);
console.log(result);