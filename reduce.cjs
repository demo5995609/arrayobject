//CallBack function creation
let arr = require('./arrays.cjs');

function reduce(elements, cb, startingValue) {
    let result = startingValue;
    let idx = 0;
    if(startingValue == undefined){
        result = elements[0];
        idx = 1;
    }
    
    // making a loop for elements
    for( ;idx<elements.length; idx++){
        // calling a callback function
        result = cb(result, elements[idx], idx, elements);
    }
    return result;
}

module.exports = reduce;