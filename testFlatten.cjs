let flatten = require('./flatten.cjs');
const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'
// const nestedArray = []; // use this to test 'flatten'
let depth = 4;
let result = flatten(nestedArray, depth);

console.log(result);