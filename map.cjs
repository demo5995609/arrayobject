let arr = require('./arrays.cjs');
function callBackFunction(num, index, arr) {
    return num*2;
}

function mapFunction(arr, cb) {
    let newArr = [];
    for(let idx = 0; idx<arr.length; idx++){
        newArr[idx] = cb(arr[idx], idx, arr);
    }
    return newArr;
}

let result = mapFunction(arr, callBackFunction);
console.log(result);

module.exports = mapFunction;